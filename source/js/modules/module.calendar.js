'use strict';

export default class Calendar {

	constructor(options) {
		this._polyfill();

		Object.assign(this._options = {}, this._default(), options);
		this._init();
	}

	_polyfill() {

		//assign
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}
	}

	_default(){
		return {
			dates: [],
			offsetWeek: 1,
			today: {
				day: null,
				week: null,
				month: null,
				year: null
			},
			current: {
				month: null,
				year: null
			},
			month: {
				short: ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'],
				full: ['январь','февраль','март','апрель','май','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь']
			},
			week: {
				short: ['вс','пн','вт','ср','чт','пт','сб'],
				full: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота']
			},
			setCalendar: function (calendar) {
				let html = '';

				html += `<div class="year">${calendar.month.full} ${calendar.year}</div>`;

				for (let i = 0; i < 7; i++){
					html+= `<div class="week">${calendar.weeks.short[i]}</div>`
				}

				for (let j = 0; j < calendar.days.length; j++){
					let cls = `${calendar.days[j].prevMonth ? 'prev' : '' } ${calendar.days[j].nextMonth ? 'next' : '' } ${calendar.days[j].today ? 'today' : '' }`;
					html+= `<div class="day ${cls}">${calendar.days[j].day}</div>`
				}

				document.getElementById('calendar').innerHTML = html;
			}
		}
	}

	_isLeapYear(year){
		return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
	}

	_amountDay(month, year){
		let d = [31, (this._isLeapYear(year || this._options.current.year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		return d[(month >= 0 ? month : this._options.current.month)];
	}

	updateDates(arr){
		this._options.dates = arr;
	}

	_setToday(){
		let now = new Date();
		this._options.today.day = now.getDate();
		this._options.today.month = now.getMonth();
		this._options.today.year = now.getFullYear();
		this._options.today.week = now.getDay();
	}

	_getMonth(){
		let calendar = {
				weeks: {
					short: [],
					full: []
				},
				month: {
					short: this._options.month.short[this._options.current.month],
					full: this._options.month.full[this._options.current.month]
				},
				days: [],
				year: this._options.current.year
			},
			amountDay = this._amountDay(),
			firstWeekMonth = new Date(this._options.current.year, this._options.current.month, 1).getDay()
		;

		for (let i = 0; i < this._options.week.short.length; i++){
			calendar.weeks.short.push(this._options.week.short[(i + this._options.offsetWeek)%7]);
			calendar.weeks.full.push(this._options.week.full[(i + this._options.offsetWeek)%7]);
		}

		for (let d = 0; d < amountDay; d++ ){
			let day = {
				day: d + 1,
				week: {
					short: this._options.week.short[(firstWeekMonth + d) % 7],
					full: this._options.week.full[(firstWeekMonth + d) % 7]
				}
			};

			if ( this._options.current.year === this._options.today.year && this._options.current.month === this._options.today.month && this._options.current.day === d + 1 ) day.today = !0;

			calendar.days.push(day)
		}

		let pm = 0,
			pmMonth = this._options.current.month - 1 < 0 ? 11 : this._options.current.month - 1,
			pmYear = pmMonth === 11 ? this._options.current.year - 1 : this._options.current.year,
			pmAmountDay = this._amountDay(pmMonth, pmYear);

		for (pm; pm < (firstWeekMonth + 7 - this._options.offsetWeek)%7; pm++){
			let pmWeek = firstWeekMonth - 1 - (pm%7) < 0 ? 6 : firstWeekMonth - 1 - (pm%7),
				day = {
					day: pmAmountDay - pm,
					week: {
						short: this._options.week.short[pmWeek],
						full: this._options.week.full[pmWeek]
					},
					prevMonth: !0
				};

			calendar.days.unshift(day);
		}

		for (let nm = 0; nm < (7 - (amountDay + firstWeekMonth)%7 + this._options.offsetWeek)%7; nm++){
			let nmWeek = (firstWeekMonth + amountDay + nm) % 7,
				day = {
					day: nm + 1,
					week: {
						short: this._options.week.short[nmWeek],
						full: this._options.week.full[nmWeek]
					},
					nextMonth: !0
				};

			calendar.days.push(day);
		}

		this._options.setCalendar(calendar);
		return calendar;
	}

	prevMonth(){
		this._options.current.month = this._options.current.month - 1 < 0 ? 11 : this._options.current.month - 1;
		this._options.current.year = this._options.current.month === 11 ? this._options.current.year - 1 : this._options.current.year;
		this._getMonth();
	}

	nextMonth(){
		this._options.current.month = this._options.current.month + 1 > 11 ? 0 : this._options.current.month + 1;
		this._options.current.year = this._options.current.month === 0 ? this._options.current.year + 1 : this._options.current.year;

		this._getMonth();
	}

	getMonth(month, year){
		this._options.current.month = month || this._options.current.month;
		this._options.current.year = year || this._options.current.year;

		this._getMonth();
	}

	todayMonth(){
		this.getMonth(this._options.today.month, this._options.today.year);
	}

	_init(){
		this._setToday();
		this.todayMonth();
	}

}