import '../css/conditer.scss'

import Inputmask from 'inputmask'
import 'select2/dist/js/select2.js';

$(function () {


	$('.form__field select').select2({
		width: '100%'
	});


	$('.form__field input, .form__field textarea').each(function (e) {
		let $input = $(this),
			$field = $input.closest('.form__field');

		setTimeout(()=>{

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		}, 100);
	});

	let $b = $('body');

	$b

		.on('blur', '.conditer .form__field input, .conditer .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('change', '.conditer .popup__conditer-item-input', function(e){
			let $t = $(this),
				$popup = $t.closest('.conditer'),
				data = $t.closest('.popup__conditer-item').attr('data-item'),
				parse
			;

			$popup.find('.popup__conditer-item').removeClass('is-active');
			$t.closest('.popup__conditer-item').addClass('is-active');

			try{
				parse = JSON.parse(data);
			} catch (e){
				console.error('ошибка парсинга', e);
			}
			if ( data && parse){
				$.each(parse, (key, val)=>{

					let $el = $(`[data-${key}]`);

					if (val && val !== false && val !== 'false'){
						$el.html(val).closest('[data-wrap]').removeClass('is-hidden');
					} else if ( val === false || val === 'false' ){
						$el.html('').closest('[data-wrap]').addClass('is-hidden');
					}

				})
			}
		})
		.on('mouseover', '.conditer .hint', function(e){
			let $t = $(this),
				$content = $t.find('.hint__content')
			;

			$t.addClass('is-hover');
			$content.removeAttr('style');

			if( $t.offset().left + $content.outerWidth(true) + 40 >  window.innerWidth ){
				$content.css({marginLeft: window.innerWidth - ($t.offset().left + $content.outerWidth(true) + 40) + 'px' })
			}

		})
		.on('mouseleave', '.conditer .hint', function(e){
			let $t = $(this),
				$content = $t.find('.hint__content')
			;
			$t.removeClass('is-hover');

		})
	;



});

