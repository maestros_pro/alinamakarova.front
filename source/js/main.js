import '../css/style.scss'

import $ from 'jquery'
import './modules/sl'
// import 'slick-carousel'
import ViewPort from './modules/module.viewport'
//-import Tab from './modules/module.tab'
import './modules/module.form'
import './modules/module.helper'
import Popup from './modules/module.popup'
import objectFitImages from 'object-fit-images';

import tippy from 'tippy.js';
import 'lightgallery'
import 'lg-video'
import 'lg-thumbnail'

import Swiper from 'swiper'
import Scroll from './modules/module.scrollnav'
import StickySidebar from 'sticky-sidebar'

window.app = window.app || {};

$(()=>{

	window.app.popup = new Popup();

	objectFitImages('.img-fit');

	let $b = $('body'), app = {}, breakpoint, breakpointCount;


	// $b.on('click', '[data-remove]', function (e) {
	// 	e.stopPropagation();
	// 	drugndrop.remove($(this).attr('data-remove'));
	// });

	new ViewPort({
		'0': ()=>{
			breakpoint = 'mobile';
			breakpointCount = 1;
		},
		'720': ()=>{
			breakpoint = 'tablet';
			breakpointCount = 2;
		},
		'950': ()=>{
			breakpoint = 'compact';
			breakpointCount = 3;
		},
		'1330': ()=>{
			breakpoint = 'desktop';
			breakpointCount = 4;
		},
		'1660': ()=>{
			breakpoint = 'fullhd';
			breakpointCount = 5;
		}
	});


	$b
		.on('mousedown', (e)=>{
			if ( $('.is-usermenu-open').length ){
				if ( !$(e.target).closest('.is-usermenu-open').length ) $('.is-usermenu-open').removeClass('is-usermenu-open');
			}
			if ( $('.is-search-show').length ){
				if ( !$(e.target).closest('.is-search-show').length ) $('.is-search-show').removeClass('is-search-show');
			}
			if ( $('.course-lessons__filter-group.is-open').length ){
				if ( !$(e.target).closest('.course-lessons__filter-group').length ) $('.course-lessons__filter-group').removeClass('is-open');
			}
		})
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			let offset = breakpoint === 'mobile' ? 140 : 20;
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top - offset}, 500);
		})
		.on('click', '.header__handler', (e)=>{
			$b.toggleClass('is-menu-open');
		})
		.on('click', '.header__cart, .cart__close', (e)=>{
			$b.toggleClass('is-cart-open');
		})
		.on('click', '.header__auth-btn', (e)=>{
			$('.header__auth').toggleClass('is-usermenu-open');
			$('.is-menu-open').removeClass('is-menu-open');
			$b.removeClass('is-cart-open');
		})
		.on('click', '.course-lessons__filter-group', function(e){
			$('.course-lessons__filter-group').not(this).removeClass('is-open');
			$(this).addClass('is-open');
		})
	;






	$('.header__search').each(function () {
		let $t = $(this),
			$clear = $t.find('.header__search-clear'),
			$lens = $t.find('.header__search-ico'),
			$form = $t.closest('form'),
			$input = $t.find('.header__search-input')
		;

		$lens.on('click', ()=>{

			if ( $input.is(':visible') ){
				if ( !!$input.val().trim() ) {
					$form.submit();
				} else {
					$input.focus();
				}
			} else {
				$t.addClass('is-search-show');
				$input.focus();
			}
		});

		$clear.on('click', ()=>{
			$input.val('').focus();
			$t.removeClass('is-input-fill')
		});

		$input.on('keyup', (e)=>{
			if ( !!e.target.value.trim() ){
				$t.addClass('is-input-fill')
			} else {
				$t.removeClass('is-input-fill')
			}
		})

	});


	$('.lessons__info-form').each(function () {
		let $t = $(this),
			$sbmt = $t.find('[type=submit]'),
			$input = $t.find('input'),
			$form = $sbmt.closest('form')
		;

		$sbmt.on('click', (e)=>{

			if ( !$input.is(':visible') ){
				e.preventDefault();
				$form.addClass('is-show');
			}
		});

	});


	$('.course-about__gallery').each(function(){
		let $t = $(this),
			$slider = $t.find('.course-about__gallery-thumb'),
			$dots = $t.find('.course-about__gallery-dots')
		;

		$slider
			.children()
			.addClass('swiper-wrapper')
			.children()
			.addClass('swiper-slide')
		;

		new Swiper($slider, {
			slidesPerView: 'auto',
			spaceBetween: 10,
			direction: 'horizontal',
			pagination: {
				el: $dots,
				clickable: true,
			},
			navigation: {
				prevEl: $t.find('.course-about__gallery-arrow_prev'),
				nextEl: $t.find('.course-about__gallery-arrow_next'),
			},
			breakpointsInverse: true,
			breakpoints: {
				720: {
					slidesPerView: 'auto',
					spaceBetween: 20,
					direction: 'horizontal',
				},
				950: {
					slidesPerView: 3,
					spaceBetween: 20,
					direction: 'vertical',
				},
			},
			on: {

				init: function () {
					// console.info('99999999999999');
					gallery($t, '.zoom');
				},
				slideChange: function () {
					// console.info('33333333');
				},
			}
		});
	});

	$('.course-feedback__list').each(function(){
		let $t = $(this),
			$slider = $t.find('.course-feedback__slider'),
			$dots = $t.find('.course-feedback__dots')
		;

		$slider
			.children()
			.addClass('swiper-wrapper')
			.children()
			.addClass('swiper-slide')
		;

		new Swiper($slider, {
			slidesPerView: 'auto',
			spaceBetween: 10,
			pagination: {
				el: $dots,
				clickable: true,
			},
			navigation: {
				prevEl: $t.find('.course-feedback__arrow_prev'),
				nextEl: $t.find('.course-feedback__arrow_next'),
			},
			breakpointsInverse: true,
			breakpoints: {
				720: {
					spaceBetween: 20,
				},
			}
		});
	});


	$('[data-tooltip-content]').each((i, e)=>{

		let $content = $($(e).attr('data-tooltip-content'));

		if ( $content.length ){
			tippy(e, {
				content: $content.html(),
				delay: [0, 100],
				theme: 'light',
				placement: 'bottom',
				arrow: true,
				interactive: true,
			})
		}

	});


	// sertificates gallery

	$('.sertificates__list').each((i, t)=>{
		let $slider = $(t)
		;

		$slider
			.on('init', function (event, slick) {
				gallery($slider, 'a');
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: false,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				// fade: true,
				variableWidth: true,
				// adaptiveHeight: true,
				prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M42,4.5c0.8,0,1.5,0.7,1.5,1.5c0,0.4-0.2,0.8-0.4,1.1L20.1,30l22.9,23c0.6,0.6,0.6,1.5,0.1,2.1s-1.5,0.6-2.1,0.1c0,0,0,0-0.1-0.1l-24-24c-0.6-0.6-0.6-1.5,0-2.1c0,0,0,0,0,0l24-24C41.2,4.6,41.6,4.5,42,4.5z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M18,55.5c-0.8,0-1.5-0.7-1.5-1.5c0-0.4,0.2-0.8,0.4-1.1L39.9,30L16.9,7c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0l24,24c0.6,0.6,0.6,1.5,0,2.1c0,0,0,0,0,0l-24,24C18.8,55.3,18.4,55.5,18,55.5z"/></svg></div>',
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 950,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 1330,
						settings: {
							slidesToShow: 3
						}
					}
				]



			});

	});

	$('.lessons__slider').each((i, t)=>{
		let $slider = $(t)
		;

		$slider
			.on('init', function (event, slick) {
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: false,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				// fade: true,
				variableWidth: true,
				// adaptiveHeight: true,
				prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M42,4.5c0.8,0,1.5,0.7,1.5,1.5c0,0.4-0.2,0.8-0.4,1.1L20.1,30l22.9,23c0.6,0.6,0.6,1.5,0.1,2.1s-1.5,0.6-2.1,0.1c0,0,0,0-0.1-0.1l-24-24c-0.6-0.6-0.6-1.5,0-2.1c0,0,0,0,0,0l24-24C41.2,4.6,41.6,4.5,42,4.5z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M18,55.5c-0.8,0-1.5-0.7-1.5-1.5c0-0.4,0.2-0.8,0.4-1.1L39.9,30L16.9,7c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0l24,24c0.6,0.6,0.6,1.5,0,2.1c0,0,0,0,0,0l-24,24C18.8,55.3,18.4,55.5,18,55.5z"/></svg></div>',
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 720,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 1330,
						settings: {
							slidesToShow: 3
						}
					}
				]
			});

	});


	$('.announce__list').each((i, t)=>{
		let $slider = $(t),
			$inner = $slider.find('.announce__slide-inslider')
		;

		$slider
			.on('init', function (event, slick) {
				gallery($slider, '.zoom');
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				// console.info(nextSlide);

				if ( nextSlide === 0 && breakpointCount > 3 ){
					$slider.slick("slickSetOption", "centerMode", false, false);
				} else  if ( breakpointCount > 3 ){
					$slider.slick("slickSetOption", "centerMode", true, false);
				}
			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: false,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				// centerMode: false,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				// fade: true,
				variableWidth: true,
				prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M42,4.5c0.8,0,1.5,0.7,1.5,1.5c0,0.4-0.2,0.8-0.4,1.1L20.1,30l22.9,23c0.6,0.6,0.6,1.5,0.1,2.1s-1.5,0.6-2.1,0.1c0,0,0,0-0.1-0.1l-24-24c-0.6-0.6-0.6-1.5,0-2.1c0,0,0,0,0,0l24-24C41.2,4.6,41.6,4.5,42,4.5z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M18,55.5c-0.8,0-1.5-0.7-1.5-1.5c0-0.4,0.2-0.8,0.4-1.1L39.9,30L16.9,7c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0l24,24c0.6,0.6,0.6,1.5,0,2.1c0,0,0,0,0,0l-24,24C18.8,55.3,18.4,55.5,18,55.5z"/></svg></div>',
				mobileFirst: true,
				/*responsive: [
					{
						breakpoint: 950,
						settings: {
							centerMode: true,
						}
					}
				]*/
			});


		$inner.each((i, t)=>{
			let $inslider = $(t);

			$inslider
				.on('touchstart touchmove mousemove mouseenter', function(e) {
					$slider.slick('slickSetOption', 'swipe', false, false);
				})
				.on('touchend mouseover mouseout', function(e) {
					$slider.slick('slickSetOption', 'swipe', true, false);
				})
				.on('init', function (event, slick) {

				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				})
				.on('afterChange', function () {

				})
				.slick({
					infinite: false,
					speed: 300,
					swipe: true,
					dots: true,
					swipeToSlide: true,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					// fade: true,
					prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
					nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
				});

		})
	});

	window.sliderState = {};

	function courseGuideGraphSlider(destroy) {
		$('.course-guide__graph-slider').each(function(i){
			let $t = $(this),
				$wrap = $t.closest('.course-guide__graph'),
				$slider = $t.find('.course-guide__graph-slider-wrap'),
				$dots = $wrap.find('.course-guide__graph-slider-dots')
			;


			if ( destroy && $slider.attr('data-stamp') ){
				window.sliderState[$slider.attr('data-stamp')].destroy(true, true);
				$slider
					.removeAttr('data-stamp')
					.children()
					.removeClass('swiper-wrapper')
					.children()
					.removeClass('swiper-slide')
				;
			} else if ( !destroy && !$slider.attr('data-stamp') ){
				let stamp = 'courseGuideStateSlider' + new Date().getTime() + '_' + i;
				$slider
					.attr('data-stamp', stamp)
					.children()
					.addClass('swiper-wrapper')
					.children()
					.addClass('swiper-slide')
				;

				window.sliderState[stamp] = new Swiper($slider, {
					slidesPerView: 1,
					// autoHeight: true,
					pagination: {
						el: $dots,
						clickable: true,
					},
					on: {
						init: function () {

						},
						slideChange: function () {
							// console.log('slide changed', this.slides[this.activeIndex]);
							let $current = $(this.slides[this.activeIndex]),
								marker = $current.attr('data-slide')
							;

							$wrap.find('[data-slide]').removeClass('is-active');
							$wrap.find(`[data-slide=${marker}]`).addClass('is-active');
						},
					}
				})
				;

			}

		});
	}

	function courseGuideStateSlider(destroy) {
		$('.course-guide__state-slider').each(function(i){
			let $t = $(this),
				$slider = $t.find('.course-guide__state-slider-wrap'),
				$dots = $t.parent().find('.course-guide__state-slider-dots')
			;

			if ( destroy && $slider.attr('data-stamp') ){
				window.sliderState[$slider.attr('data-stamp')].destroy(true, true);
				$slider
					.removeAttr('data-stamp')
					.children()
					.removeClass('swiper-wrapper')
					.children()
					.removeClass('swiper-slide')
				;
			} else if ( !destroy && !$slider.attr('data-stamp') ){
				let stamp = 'courseGuideStateSlider' + new Date().getTime() + '_' + i;
				$slider
					.attr('data-stamp', stamp)
					.addClass('slider-init')
					.children()
					.addClass('swiper-wrapper')
					.children()
					.addClass('swiper-slide')
				;

				window.sliderState[stamp] = new Swiper($slider, {
					slidesPerView: 'auto',
					spaceBetween: 20,
					pagination: {
						el: $dots,
						clickable: true,
					},
				});
			}

		});
	}

	$b.on('click', '.course-guide__graph-slider-item', function () {
		let $t = $(this),
			$wrap = $t.closest('.course-guide__graph'),
			marker = $t.attr('data-slide')
		;

		if ( breakpointCount > 2){
			$wrap.find('[data-slide]').removeClass('is-active');
			$wrap.find(`[data-slide=${marker}]`).addClass('is-active');
		}

	}).on('click', '.course-about__gallery-thumb-item', function () {
		let $t = $(this),
			$wrap = $t.closest('.course-about__gallery'),
			marker = $t.attr('data-slide')
		;

		if ( breakpointCount > 2){
			$wrap.find('[data-slide]').removeClass('is-active');
			$wrap.find(`[data-slide=${marker}]`).addClass('is-active');
		} else {
			$wrap.find(`.course-about__gallery-main-item[data-slide=${marker}]`).find('.zoom').click();
		}

	});

	new ViewPort({
		'0': ()=>{
			courseGuideGraphSlider();
			courseGuideStateSlider();
		},
		'950': ()=>{
			courseGuideGraphSlider(true);
			courseGuideStateSlider(true);
		}
	});


	scrollfix();
	$(window).on('scroll', ()=>{scrollfix()});
	$(window).on('resize', ()=>{scrollfix(true)});

	function scrollfix(reset) {
		let headerHeight = 100;
		if ( window.pageYOffset > headerHeight && !$b.hasClass('is-menu-fixed') ){
			$b.addClass('is-menu-fixed');
		} else if (window.pageYOffset <= headerHeight && $b.hasClass('is-menu-fixed')) {
			$b.removeClass('is-menu-fixed');
		}

		let $menu = $('.menu');

		if ( $menu.length ){
			let $header = $('.header'),
				$inner = $menu.find('.menu__list'),
				offset = $header.height(),
				top = $menu.offset().top
			;

			if ( reset ) $menu.removeClass('is-fixed');

			if ( window.pageYOffset > top - offset && !$menu.hasClass('is-fixed')){
				$menu.addClass('is-fixed');
				$inner.css({top: offset})
			} else if (window.pageYOffset <= top - offset && $menu.hasClass('is-fixed')) {
				$menu.removeClass('is-fixed');
				$inner.removeAttr('style');
			}
		}

	}


	function gallery(wrap, item) {

		$(wrap).each((i, e)=>{
			//-console.info(e);
			$(e).eq(i).lightGallery({
				counter: false,
				download: false,
				loop: true,
				hideControlOnEnd: true,
				selector: item,
				youtubePlayerParams: {
					modestbranding: 1,
					showinfo: 0,
					rel: 0,
					controls: 0
				},
				vimeoPlayerParams: {
					byline : 0,
					portrait : 0,
					color : 'A90707'
				},

				thumbnail:true,
				// dynamic:true,
			})
		});

	}


	// scroll navigation



	$('.menu').each(function () {

		let $t = $(this),
			$item = $t.find('[data-section]'),
			sections = [],
			activeMenu = null;

		$item.each(function () {
			let name = $(this).attr('data-section');

			sections.push({
				element: `.${name}`,
				name: name,
			});

			$(this).click((e)=>{
				e.preventDefault();
				let offset = 120;
				$('html,body').animate({scrollTop: $(`.${name}`).offset().top - offset}, 500);
			})
		});

		new Scroll({
			sections,
			offset: 140,
			onChange: (e, i)=>{
				//-console.info('onChange', e, i);

				if ( activeMenu !== e.name){
					activeMenu = e.name;

					$('[data-section]').removeClass('is-active');
					$('[data-section=' + activeMenu + ']').addClass('is-active');
				}
			},
			onOut: (e)=>{

			}
		});
	});


	setTimeout(()=>{

		if ( $('.sticky-target').length){

			let offset = 20;

			if ( $('.header').length ) offset += $('.header').height();
			if ( $('.menu__list').length ) offset += $('.menu__list').height();

			new StickySidebar('.sticky-target', {
				topSpacing: offset,
				bottomSpacing: 0,
				containerSelector: '.sticky-frame',
				innerWrapperSelector: '.sticky-inner',

				resizeSensor: true,
				minWidth: 0
			});
		}
	}, 100)


});

