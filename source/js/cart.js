import Vue from 'vue';



//Vue.config.devtools = location.search.indexOf('vuedebug') >= 0;
Vue.config.devtools = true;

import store from '../js/vue/vuex'
import '../js/vue/filters.js'

import Api from '../js/vue/api'
Vue.use(Api);

import VTooltip from 'v-tooltip'
Vue.use(VTooltip);

import VueInputMask from 'vue-inputmask'
Vue.use(VueInputMask.default);

import VueScrollbar from 'vue-scrollbar-live';
Vue.component('scrollbar', VueScrollbar);

import cartContent from '../view/cart-content.vue';
import cartAside from '../view/cart-aside.vue';
import cartBanners from '../view/cart-banners.vue';
import cartOunside from '../view/cart-outside.vue';

if ( document.getElementById('cartContent') ){
	new Vue({
		el: '#cartContent',
		filters: {},
		// router,
		store,
		render: h => h(cartContent)
	});
}

if ( document.getElementById('cartAside') ){
	new Vue({
		el: '#cartAside',
		filters: {},
		// router,
		store,
		render: h => h(cartAside)
	});
}

if ( document.getElementById('cartBanners') ){
	new Vue({
		el: '#cartBanners',
		filters: {},
		// router,
		store,
		render: h => h(cartBanners)
	});
}

if ( document.getElementById('cartOutSide') ){
	new Vue({
		el: '#cartOutSide',
		filters: {},
		// router,
		store,
		render: h => h(cartOunside)
	});
}

