import axios from 'axios'

const apiUrl = window.cartApi;

axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
axios.defaults.transformRequest = [function (data) {

	let formData = new FormData();

	Object.keys(data).map(function(key) {
		if (Array.isArray(data[key])) {

			for (let i = 0; i < data[key].length; i++) {
				formData.append(`${key}[]`, typeof data[key][i] === 'object' ?  JSON.stringify(data[key][i]) : data[key][i]);
			}

		} else {
			formData.append(key, data[key]);
		}
	});

	return formData;
}];

let request = (url, data={}, response=()=>{}, error=()=>{}, always=()=>{}, progress=()=>{})=>{
	if (isLocal) console.info('request', data);

	if ( typeof url === 'string' && url.indexOf('/') < 0 ){
		data.action = url;
		url = apiUrl;
	} else if ( typeof url === "object"){
		progress = always;
		always = error;
		error = response;
		response = data;
		data = url;
		url = apiUrl
	}

	return axios({
			method: 'post',
			url: url,
			data: data,
			onUploadProgress: progress
		}).then(e=>{
			if (e.data.reload) window.location.reload();
			response(e.data);
		}).catch(error).then(()=>{
			window.dispatchEvent(new Event('resize'));
			always();
		});
	}
;

export default {
	install: (Vue, options)=>{
		Vue.prototype.$api = request;
	}
};
