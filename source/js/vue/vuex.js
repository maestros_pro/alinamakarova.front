import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {

		step: 1,

		moreInfo: {

			error: {

			},

			model: {

			}
		},

		gifts: {
			list: [],
			model: '',
			title: '',
			disabled: false,
			disabledMessage: '',
		},

		banners: [

		],

		paymentId: null,

		cart: {
			loaded: false,
			detail: {
				orderId: null,
				count: 0,
				cost: 0,
				discount: 0,
			},
			discounts: [],
			products: [],
			needMoreInfo: false,
			needMoreOptions: null,
		},

		user: {
			loaded: false,
			status: false,
			name: '',
			email: '',
			backgroundColor: false,
			photo: false,
		},
	},

	actions: {
		loadUser(context){
			this._vm.$api(
				'getUserInfo',
				{},
				(data)=>{
					context.commit('updateUser', data );
				},
				(err)=>{
					console.error(err);
				},
			);
		},

		loadProduct(context){
			this._vm.$api(
				'getCartProducts',
				{},
				(data)=>{
					context.commit('updateProduct', data );
				},
				(err)=>{
					console.error(err);
				},
			);
		},

		loadBanners(context){
			this._vm.$api(
				'getCartBanners',
				{},
				(res)=>{
					context.commit('updateBanners', res );
				},
				(err)=>{
					console.error(err);
				},
			);
		},

		loadGift(context){
			this._vm.$api(
				'getPresentList',
				{},
				(res)=>{
					context.commit('setPresentList', res );
				},
				(err)=>{
					console.error(err);
				},
			);
		},
	},

	mutations: {
		updateUser(state, data){
			Object.assign(state.user, data);
			state.user.loaded = true;

			if ( data.step ) state.user = data.step;
		},

		updateBanners(state, data){
			if ( Array.isArray(data.banners) ){
				state.banners = data.banners;
			} else {
				state.banners = [];
			}
		},

		setPresentList(state, data){
			if ( data.status && data.gifts && data.gifts.length ){
				state.gifts.title = data.title;
				state.gifts.list = data.gifts;
				state.gifts.disabled = data.disabled;
				state.gifts.disabledMessage = data.disabledMessage;
			} else {
				state.gifts.title = '';
				state.gifts.list = [];
				state.gifts.disabled = false;
				state.gifts.disabledMessage = '';
			}
		},

		updateProduct(state, data){
			state.cart.loaded = true;
			let _default = {
				detail: {
					orderId: null,
					count: 0,
					cost: 0,
					discount: 0,
				},
				discounts: [],
				products: [],
				needMoreInfo: false,
				needMoreOptions: null,
			};
			Object.assign(state.cart, _default, data);

			if ( data.needMoreOptions && data.needMoreOptions.fields && data.needMoreOptions.fields.length){
				data.needMoreOptions.fields.forEach((item => {
					this._vm.$set(state.moreInfo.model, item.name, '');
					this._vm.$set(state.moreInfo.error, item.name, '');
				}));
			}

			let topCartText = document.querySelectorAll('.header__cart-text')[0];

			if ( !!topCartText ){
				if ( state.cart.detail.count && state.cart.detail.count > 0 ){
					topCartText.innerHTML = `Корзина (${state.cart.detail.count})`
				}  else {
					topCartText.innerHTML = `Корзина`
				}
			}
		},

		goToStep(state, number){
			state.step = number;
		},

		setPayment(state, number){
			state.paymentId = number;
		},

		setMoreInfoError(state, data){
			console.info('setMoreInfoError', data, state.moreInfo.error, state.moreInfo.error[data.name] );
			if ( state.moreInfo.error[data.name] !== undefined ) {
				state.moreInfo.error[data.name] = data.text;
			}
			console.info('setMoreInfoError 111', state.moreInfo.error[data.name] );
		},
	},

	getters: {
		isMoreInfo(state){
			let res = true;

			if (state.cart.needMoreInfo && state.cart.needMoreOptions) {
				Object.keys(state.moreInfo.model).map(function(key) {
					if ( !state.moreInfo.model[key] ){
						res = false;
					}
				});
			}

			return res;
		},

	}
});



