import Vue from "vue";


function declension(value, args) {
	let cases = [2, 0, 1, 1, 1, 2],num = Number(value);
	if ( isNaN(num) ){
		console.error('declension must be a Number');
		return value;
	}
	else {
		return args[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]];
	}
}

/**
 * {{ '1000000000000' | discharge('*') }}
 * */
Vue.filter('discharge', (value, mark = ' ') => {
	return String(value).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, `$1${mark}`);
});


/**
 * {{ 100001 | declension('день', 'дня', 'дней') }}
 * */
Vue.filter('declension', (value, ...args) => {
	let cases = [2, 0, 1, 1, 1, 2],num = Number(value);
	if ( isNaN(num) ){
		console.error('declension must be a Number');
		return value;
	}
	else {
		return args[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]];
	}
});


/**
 * {{ asdasdasdasd | substrLengh(10, '...') }}
 * */
Vue.filter('substrLengh', (value, limit, postfix = '...', minLastLetter = 2) => {

	if ( !limit ) return value;

	let text = value.toString().trim(), arr, lastSpace;
	if( text.length <= limit) return text;
	text = text.slice( 0, limit);
	arr = text.split(' ');
	lastSpace = text.lastIndexOf(' ');
	if ( arr[arr.length - 1].length < minLastLetter && lastSpace > 0 ){
		text = text.substr(0, lastSpace);
	}
	return text + postfix;
});


