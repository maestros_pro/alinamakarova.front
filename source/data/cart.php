<?php
header('Access-Control-Allow-Origin: *');
sleep(0.8);

$data = array();

$action = $_POST['action']; // метод запроса {{string}}

switch ($action) {

	case 'getUserInfo':	// загрузка информации о пользователе

		/**
		 * Если авторизован, то true, ессли не авторизован, то false
		 */
		$data['status'] = false;

		/**
		 * Если пользователь авторизован, добавляем информацию о пользователе
		 */

//		$data['status'] = true;
//		$data['name'] = 'Дуся Антонова';
//		$data['email'] = 'pochta@pochta.ru';
//		$data['backgroundColor'] = '#ff00ff';
//		$data['photo'] = 'https://via.placeholder.com/200x200/43515e/43515e';

		break;

	case 'getCartProducts':	// загрузка продуктов в карзине

		$data = array();

		$data['detail'] = array(
			'orderId' => 1231321,	// номер заказа
			'count' => 4,		// количетво курсов к корзине
			'cost' => 2360,		// стоимость без учета скидки
			'discount' => 250,	// размер скидки
		);

		/**
		 * Если если добавлен мегакурс (запрос на дополнительные поля)
		 */
		$data['needMoreInfo'] = true;

		$data['needMoreOptions'] = array(
			"fields" => array(
				array(
					"name" => "name",
					"placeholder" => "Имя и Фамилия",
				),
				array(
					"name" => "phone",
					"placeholder" => "Телефон",
				),
				array(
					"name" => "instagram",
					"placeholder" => "Ваша страничка в Instagram",
				),
				array(
					"name" => "address",
					"placeholder" => "Адрес (куда отправить ваш сертификат)",
				),

			),
			"note_message" => "Заполняйте поля корректно! Эти данные мы будем использовать для ваших сертификатов и для включения в чат WhatsApp"
		);

		// список активных скидок
		$data['discounts'] = array(
			array(
				'id' => 110001,
				'name' => 'Мегакурс CAKE',
				'value' => 250,
			),
			array(
				'id' => 110002,
				'name' => 'Скидка на курс "Земляника со сливками"',
				'value' => 250,
			)
		);

		$data['products'] = array (
			array (
				'id' => 1111,
				'title' => 'ФУД-ФОТО ДЛЯ КОНДИТЕРА',
				'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
				'priceOld' => 4590,
				'price' => 3590,
				'stat' => array (
					'level' => 'medium',			// сложность easy medium hard
					'duration' => '32 минуты',		// длительность
					'volume' => '12 страниц',		// объем
				),
				'added' => array (		// если к курсу предложен в подарок другой курс
					array(
						'id' => 1112,
						'title' => 'Манго',
						'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
						'stat' => array (
							'level' => 'easy',
							'duration' => '21 минута',
							'volume' => '5 страниц',
						),
						'priceOld' => 4590,
						'price' => 0,
					)
				)
			),

			array (
				'id' => 11121,
				'title' => 'База знаний',
				'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
				'stat' => array (
					'level' => 'hard',
					'duration' => '100 минут',
					'volume' => '40 страниц',
				),
				'priceText' => 'Подписка на&nbsp;1&nbsp;месяц',
				'price' => 290,
			),

			array (
				'id' => 11311,
				'title' => 'Кондитер 2.0',
				'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
				'stat' => array (
					'level' => 'medium',
					'duration' => '100 минут',
					'volume' => '40 страниц',
				),
				'priceText' => 'Тариф стандарт',
				'price' => 17900,
			),


			array (
				'id' => 789456,
				'title' => 'Кондитер 2.0',
				'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
				'stat' => array (
					'level' => 'medium',
					'duration' => '100 минут',
					'volume' => '40 страниц',
				),
				'priceText' => 'Тариф стандарт',
				'price' => 17900,
			),


			array (
				'id' => 23423,
				'title' => 'ФУД-ФОТО ДЛЯ КОНДИТЕРА',
				'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
				'priceOld' => 4590,
				'price' => 3590,

				'option' => array(
					array(
						'inputs' => array(
							array(
								'id' => 100001,
								'name' => 'Подписка на 1 месяц',
								'tooltip' => 'Какая подсказка',
								'priceOld' => 490,
								'price' => 290,
								'monthly' => false,
								'partial' => false,
								'value' => 'База знаний',
								'checked' => true
							),
							array(
								'id' => 100002,
								'name' => 'Подписка на 1 год',
								'tooltip' => 'Какая подсказка',
								'priceOld' => 4290,
								'price' => 3290,
								'monthly' => false,
								'partial' => true,
								'value' => 'База знаний',
							)
						)
					),
				),
			),


			array (
				'id' => 4234234,
				'title' => 'ФУД-ФОТО ДЛЯ КОНДИТЕРА',
				'image' => 'https://via.placeholder.com/400x300/43515e/43515e',
				'priceOld' => 4590,
				'price' => 3590,

				'option' => array(
					array(
						'title' => 'Тариф: <span class="c-yellow">Стандарт</span>',
						'tooltip' => 'Какая подсказка к тарифу стандарт',
						'inputs' => array(
							array(
								'id' => 100003,
								'name' => 'Оплатить сразу',
								'priceOld' => 2462,
								'price' => 1790,
								'monthly' => true,
								'partial' => false,
								'value' => 'Кондитер 2.0',
							),
							array(
								'id' => 100004,
								'name' => 'В рассрочку',
								'priceOld' => 24620,
								'price' => 17900,
								'monthly' => false,
								'partial' => true,
								'value' => 'Кондитер 2.0',
							)
						)
					),
					array(
						'title' => 'Тариф: <span class="c-yellow">Бизнес</span>',
						'tooltip' => 'Какая подсказка к тарифу бизнес',
						'inputs' => array(
							array(
								'id' => 100005,
								'name' => 'Оплатить сразу',
								'priceOld' => 2462,
								'price' => 1790,
								'monthly' => true,
								'partial' => false,
								'value' => 'Кондитер 2.0 бизнес',
							),
							array(
								'id' => 100006,
								'name' => 'В рассрочку',
								'priceOld' => 24620,
								'price' => 17900,
								'monthly' => false,
								'partial' => true,
								'value' => 'Кондитер 2.0 бизнес',
								'checked' => true
							)
						)
					),
				),
			),

		);

		break;

	case 'getCartBanners':	// загрузка файлов

		$data['banners'] = array (
			array (
				'title' => 'Бонус за регистрацию на сайте',
				'id' => 111,
				// для баннеров с картинкой на весь фон, а для баннеров с картинкой слева вместо background задать image
				'background' => 'https://via.placeholder.com/500x300/43515e/43515e',
				'textColor' => '#ffffff',
				'name' => '<span class="c-yellow">Курс за 10 рублей:</span> CAKE-POPS',
				'stat' => array (
					'level' => 'medium',
					'duration' => '100 минут',
					'volume' => '40 страниц',
				),
				'price' => 10,
				'monthly' => false,
			),
			array (
				'title' => 'Специальная цена на Базу знаний!',
				'textSmall' => 'Цена только',
				'textBig' => 'в корзине',
				'id' => 222,
				'image' => 'https://via.placeholder.com/400x300/e8a082/e8a082',
				'textColor' => '#ffffff',
				'name' => 'База знаний',
				'text' => 'Всего за 12 рублей вы получаете доступ к библиотеке, в которой собрано более 1000 полезных статей на кондитерскую тему. Каждую неделю мы пополняем Базу новыми крутыми статьями.',

				'priceOld' => 490,
				'price' => 290,
				'monthly' => true,	// сумма указана за месяц
			),
			array (
				'title' => 'Специальная цена на Базу знаний!',
				'textSmall' => 'Цена только',
				'textBig' => 'в корзине',
				'id' => 333,
				'image' => 'https://via.placeholder.com/400x300/e8a082/e8a082',
				'textColor' => '#ffffff',
				'name' => 'База знаний',
				'text' => 'Всего за 12 рублей вы получаете доступ к библиотеке, в которой собрано более 1000 полезных статей на кондитерскую тему. Каждую неделю мы пополняем Базу новыми крутыми статьями.',
				'option' => array(
					array(
						'inputs' => array(
							array(
								'id' => 100001,
								'name' => 'Подписка на 1 месяц',
								'tooltip' => 'Какая подсказка',
								'priceOld' => 490,
								'price' => 290,
								'monthly' => false,
								'partial' => false,
								'value' => 'База знаний',
							),
							array(
								'id' => 100002,
								'name' => 'Подписка на 1 год',
								'tooltip' => 'Какая подсказка',
								'priceOld' => 4290,
								'price' => 3290,
								'monthly' => false,
								'partial' => true,
								'value' => 'База знаний',
							)
						)
					),
				),
				'priceOld' => 490,
				'price' => 290,
				'monthly' => true,
			),
			array (
				'title' => 'Скидка! Кондитер 2.0',
				'textSmall' => 'Цена только',
				'textBig' => 'в корзине',
				'id' => 333,
				'image' => 'https://via.placeholder.com/400x300/dedbdc/dedbdc',
				'name' => 'Кондитер 2.0',
				'period' => array(
					'start' => '11 февраля 2019',
					'duration' => '14 недель',
				),
				'option' => array(
					array(
						'title' => 'Тариф: <span class="c-yellow">Стандарт</span>',
						'tooltip' => 'Какая подсказка к тарифу стандарт',
						'inputs' => array(
							array(
								'id' => 100003,
								'name' => 'Оплатить сразу',
								'priceOld' => 2462,
								'price' => 1790,
								'monthly' => true,
								'partial' => false,
								'value' => 'Кондитер 2.0',
							),
							array(
								'id' => 100004,
								'name' => 'В рассрочку',
								'priceOld' => 24620,
								'price' => 17900,
								'monthly' => false,
								'partial' => true,
								'value' => 'Кондитер 2.0',
							)
						)
					),
					array(
						'title' => 'Тариф: <span class="c-yellow">Бизнес</span>',
						'tooltip' => 'Какая подсказка к тарифу бизнес',
						'inputs' => array(
							array(
								'id' => 100005,
								'name' => 'Оплатить сразу',
								'priceOld' => 2462,
								'price' => 1790,
								'monthly' => true,
								'partial' => false,
								'value' => 'Кондитер 2.0 бизнес',
							),
							array(
								'id' => 100006,
								'name' => 'В рассрочку',
								'priceOld' => 24620,
								'price' => 17900,
								'monthly' => false,
								'partial' => true,
								'value' => 'Кондитер 2.0 бизнес',
							)
						)
					),
				),
				'priceOld' => 8200,
				'price' => 5967,
				'monthly' => true,
			),
		);

		break;

	case 'getPaymentList':	// загрузка файлов

		$data['payments'] = array (
			array(
				'id' => 5001,
				'name' => 'Банковской картой',
				'image' => '/img/payment/card.png',
				'tooltip' => 'Какая то подсказка'
			),
			array(
				'id' => 5002,
				'name' => 'Оплата в платежной системе «Яндекс.Деньги»',
				'image' => '/img/payment/yandex.png',
				'tooltip' => 'Какая то подсказка'
			),
			array(
				'id' => 5003,
				'name' => 'Оплата в платежной системе Web Money',
				'image' => '/img/payment/webmoney.png',
				'tooltip' => 'Какая то подсказка'
			),
			array(
				'id' => 5004,
				'name' => 'Оплата в платежной системе Qiwi wallet',
				'image' => '/img/payment/qiwi.png',
				'tooltip' => 'Какая то подсказка'
			),
			array(
				'id' => 5005,
				'name' => 'Оплата через Альфа-Клик',
				'image' => '/img/payment/alfa.png',
				'tooltip' => 'Какая то подсказка'
			),
			array(
				'id' => 5006,
				'name' => 'Наличными через терминал',
				'image' => '/img/payment/cash.png',
				'tooltip' => 'Какая то подсказка'
			),
			array(
				'id' => 5007,
				'name' => 'Интернет-банкинг Промсвязьбанка',
				'image' => '/img/payment/psb.png',
				'tooltip' => 'Какая то подсказка'
			),
		);

		break;

	case 'emailSearch':	// Поиск email

		$email = $_POST['email'];

		$data['list'] = array (
			$email . '@mail.ru',
			$email . '@gmail.com',
			$email . '@yandex.ru',
			$email . '@pochta.ru',
			$email . '@qwerty.com',
			$email . '@123.ru',
		);
		break;

	case 'removeProduct':	// Удаление продукта из карзины

		$data['status'] = true;
		break;

	case 'correctProduct':	// Изменение значения продукта из карзины

		$data['status'] = true;
		break;

	case 'addBannerItem':	// Добавление продукта кликом по баннеру

		$data['status'] = true;
		break;

	case 'logOut':	// Добавление продукта кликом по баннеру

		$data['status'] = true;
		break;

	case 'removeDiscount':	// Удаление скидки

		$data['status'] = true;
		break;

	case 'activatePromoCode':	// Активация промо кода

		// Если все норм то
		$data['status'] = true;
		// Если ошибка, то
		$data['status'] = false;
		$data['error'] = 'Такого промо кода не существует или он утратил свою актуальность.';

		break;

	case 'emailCheck':	// Проверка email

		// Если все норм то
		$data['status'] = true;
//		$data['register'] = true;
		$data['newUser'] = true;
		//$data['overlimit'] = true; // если превышен лимит
		$data['emailInfo'] = 'Мы отправим инструкции на вашу почту, после оплаты.';
		// Если ошибка в email для исправления выслать параметр
		// $data['correct'] = true;
		// Если ошибка, то
//		$data['status'] = false;
//		$data['error'] = 'Такого мыла не зарегестрировано.';

		break;

	case 'emailRecover':	// Проверка email

		// Если все норм то
		$data['status'] = true;
		$data['message'] = 'Мы отправили на вашу почту новый пароль. <br>Ждите посылку почтой России.';
		//$data['overlimit'] = true; // если превышен лимит
		// Если ошибка, то
//		$data['status'] = false;
//		$data['error'] = 'Такого мыла не зарегестрировано.';

		break;

	case 'getOrder':	// переход к оформлению

		$data['status'] = true;
		break;

	case 'logIn':	// авторизация

		$data['status'] = true;

		break;

	case 'getPay':	// оплата
		// Тут я не знаю как мне обрабатывать ответ. Возможно надо кудато редиректить?
		$data['status'] = true;
		$data['reloadUrl'] = 'https://ru.wikipedia.org/wiki/Служебная:Случайная_страница';

		// сюда же приходят данные по пользователя которые запрашиваются если нужно дополнительные данные.
		// Если в них какая то ошибка, то ответ будет такой
		//

	/*
		$data['status'] = false;
		$data['errors'] = array(
			array(
				'name' => 'name',
				'text' => 'Ошибка имени',
			),
			array(
				'name' => 'phone',
				'text' => 'Ошибка телефона',
			),
			array(
				'name' => 'instagram',
				'text' => 'Ошибка инстаграма',
			),
			array(
				'name' => 'address',
				'text' => 'Ошибка адреса',
			),
		);
*/

		break;


	case 'getPresentList':
		$data['status'] = true;

		$data['title'] = 'Выбири свой подарок!';

		$data['disabled'] = true;
		$data['disabledMessage'] = 'Вы плохо вели себя в этом году и не заслужили подарок';

		$data['gifts'] = array(
			array(
				"name" => "Подарок 1",
				"id" => "1",
				"value" => "value1",
			),
			array(
				"name" => "Подарок 2",
				"id" => "2",
				"value" => "value2",
			),
			array(
				"name" => "Подарок 3",
				"id" => "3",
				"value" => "value3",
			),
		);

		break;
}


echo json_encode($data);
?>